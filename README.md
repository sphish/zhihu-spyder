# Zhihu-Spyder

# Introduction
这是用Golang开发的爬取知乎用户信息的爬虫，爬虫项目维持一个用户Id队列，并发地从队列中取出用户Id，爬取与解析DOM结构，获取用户信息，存入MySQL数据库，并将用户的followers和followees的Id加入队列中

### 流程图

![task](pics/task.png)

### 代码结构
```c++
.
├── client
│   ├── client.go
│   ├── client_test.go
│   ├── proxy_.json
│   ├── proxy.json
│   ├── proxy_test.py
│   └── test_proxy.py
├── crawler
│   ├── crawler.go
│   └── crawler_test.go
├── dao
│   ├── connect.go
│   ├── create.go
│   └── user_dao.go
├── data_process
│   ├── processed_data
│   ├── read.py
│   └── write.py
├── main.go
├── main_test.go
├── pics
├── README.md
├── report.md
├── script
│   └── create.sql
├── types
│   └── types.go
├── util
│   ├── env.go
│   ├── parallelizer.go
│   └── parallelizer_test.go
└── xiciip
    ├── scrapy.cfg
    └── xiqiip
        ├── available.json
        ├── entrypoint.py
        ├── filter.py
        ├── __init__.py
        ├── ips.json
        ├── items.py
        ├── middlewares.py
        ├── pipelines.py
        ├── settings.py
        └── spiders
```
其中以'_test'结尾的文件是测试代码
## Crawler
爬虫的主要部分

## Script
创建MySQL数据库

## Types
利用Gorm定义数据类型

## Dao
与MySQL建立连接并利用Gorm进行数据类型的转换

## Util
环境变量
实现并发

## Client
为了防止反爬虫，设置代理ip

## Xiciip
利用scrapy框架，在[http://www.xicidaili.com](http://www.xicidaili.com)上爬取免费代理ip

![scrapy_arch](pics/scrapy_architecture.png)

* 在items.py中利用scrapy的Item对象定义数据类型，也就是需要爬取的ip,port,http字段
* xiqiip.py是主要的爬虫部分，利用Request请求获取网页信息，用正则表达式匹配字段，返回爬取的对象
* 在pipelines.py中处理爬取的对象，测试爬到的代理ip是否可用，将可用的写入ips.json
* 为了避免网站的反爬虫，在middlewares.py--中间组件--中配置了request headers和proxy ip
* settings.py是相关的一些设置

## Data_process

在将数据存储到MySQL中之后，将数据导出为csv格式。然后在read.py和write.py中做一些简单的处理，写入processed_data中。

对处理之后得到的文件，用Excel画出简单的可视化图表：分析知乎用户的性别比例，从事的工作，就读的大学，工作的公司，然后统计获得点赞、感谢和关注者最多的一些用户。

# Usage
## MySQL
默认配置为MySQL地址为localhost:3306，用户名密码为root:root123，需自行创建zhihu-spyder database
```sql
create DATABASE zhihu-spyder;
```
## proxy
将proxys的URL填在client/proxy.json中即可

P.S. 空URL代表不使用代理
## Parameters
使用环境变量配置运行时参数，可配置参数如下:
| Name             | Meaning                                          | Default |
| ---------------- | ------------------------------------------------ | ------- |
| START_USER       | 爬虫起始用户ID，建议每次重启时更换               |         |
| FILTER_SIZE      | Cuckoo Filter的最大容量                          | 800000  |
| QUEUE_SIZE       | UserId Queue的最大长度，超出最大长度时会阻塞进程 | 200000  |
| CONCURRENCY      | 并发goroutine数量                                | 3       |
| log_level        | log等级                                          | info    |
| REQUEST_INTERVAL | 请求间隔(ms)                                     | 200ms   |


