package client

import (
	"crypto/tls"
	"encoding/json"
	"net/http"
	"net/url"
	"os"
	"sync"
	"time"

	"github.com/Sirupsen/logrus"
)

var (
	proxys map[string]bool
	mutex  sync.RWMutex
)

func loadProxys() (res map[string]bool) {
	res = map[string]bool{}
	file, err := os.Open("client/proxy.json")
	if err != nil {
		logrus.Errorf("Open proxy.json error: %v", err)
		return
	}

	var proxys []string
	decoder := json.NewDecoder(file)
	if err = decoder.Decode(&proxys); err != nil {
		logrus.Errorf("Parse proxy.json error: %v", err)
		file.Close()
		return
	}
	file.Close()

	for _, proxy := range proxys {
		res[proxy] = true
	}
	return
}

func init() {
	go func() {
		tmp := loadProxys()
		mutex.Lock()
		proxys = tmp
		proxys[""] = true
		mutex.Unlock()
		time.Sleep(10 * time.Second)
	}()
}

func getProxyClient(proxyUrl string) (*http.Client, error) {
	if proxyUrl == "" {
		return &http.Client{}, nil
	}
	proxy, err := url.Parse(proxyUrl)
	if err != nil {
		logrus.Errorf("Parse proxy Url error: %v", err)
		return nil, err
	}

	tr := &http.Transport{
		Proxy:           http.ProxyURL(proxy),
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{
		Transport: tr,
		Timeout:   5 * time.Second,
	}

	return client, nil
}

func getRandomUrl() string {
	for p, _ := range proxys {
		return p
	}
	return ""
}

func getAvailableUrl() string {
	for {
		for {
			mutex.RLock()
			if len(proxys) == 0 {
				logrus.Errorf("No proxys!")
				time.Sleep(10 * time.Second)
			}
			url := getRandomUrl()
			mutex.RUnlock()
			cli, err := getProxyClient(url)
			if err != nil {
				delete(proxys, url)
				break
			}
			_, err = cli.Get("https://www.zhihu.com")
			if err != nil {
				delete(proxys, url)
				break
			}
			return url
		}
	}
}

func GetClient() (*http.Client, error) {
	proxyUrl := getAvailableUrl()
	return getProxyClient(proxyUrl)
}
