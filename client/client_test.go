package client

import (
	"testing"
)

func Test_GetClient(t *testing.T) {
	//	os.Setenv("HTTP_PROXY", "http://54.187.52.159:80")
	//	os.Setenv("HTTPS_PROXY", "http://54.187.52.159:80")
	client, err := GetClient()
	if err != nil {
		t.Error(err)
	}
	_, err = client.Get("https://www.zhihu.com")
	if err != nil {
		t.Error(err)
	}
}
