package crawler

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"strconv"
	"time"
	"zhihu-spyder/types"
	"zhihu-spyder/util"

	"github.com/PuerkitoBio/goquery"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/html"
)

//type Page struct {
//	IsEnd    bool
//	Totals   int64
//	Previous string
//	IsStart  bool
//	Next     string
//}
//
//type PeopleData struct {
//	AvatarTemplate string
//	Name           string
//	Headling       string
//	Gender         int64
//	UserType       string
//	UrlToken       string
//	IsAdvertiser   bool
//	Avatar         string
//	IsOrg          bool
//	Type           string
//	Url            string
//	Badge          []interface{}
//	Id             string
//}
//type Followees struct {
//	Paging Page
//	Data   []PeopleData
//}

func sleepRandom() {
	t := util.EnvInt("REQUEST_INTERVAL", 200)
	t += int32(rand.Float64() * float64(t))
	time.Sleep(time.Duration(t) * time.Millisecond)
}

func GetUserFollowees(userId string, client *http.Client) (res []string) {
	logrus.Infof("Start crawling user:%v followees.", userId)
	for url := "https://www.zhihu.com/api/v4/members/" + userId + "/followees?"; ; {
		sleepRandom()
		resp, err := client.Get(url)
		if err != nil {
			logrus.Errorf("Get user: %v followees error", userId)
			return
		}

		defer resp.Body.Close()
		var r map[string]interface{}
		err = json.NewDecoder(resp.Body).Decode(&r)
		if err != nil {
			logrus.Errorf("Parse user: %v followees error", userId)
			return
		}

		data, ok := r["data"].([]interface{})
		if !ok {
			logrus.Errorf("Parse user: %v followees error", userId)
			return
		}
		for _, u := range data {
			user, ok := u.(map[string]interface{})
			if !ok {
				logrus.Errorf("Parse user: %v followees error", userId)
				return
			}
			res = append(res, user["url_token"].(string))
		}

		paging, ok := r["paging"].(map[string]interface{})
		if !ok {
			logrus.Errorf("Parse user: %v followees error", userId)
			return
		}
		if paging["is_end"].(bool) == true {
			break
		}
		url, ok = paging["next"].(string)
		if !ok {
			logrus.Errorf("Parse user: %v followees error", userId)
			return
		}
	}
	return
}

func GetUserFollowers(userId string, client *http.Client) (res []string) {
	logrus.Infof("Start crawling user:%v followers.", userId)
	for url := "https://www.zhihu.com/api/v4/members/" + userId + "/followers?"; ; {
		sleepRandom()
		resp, err := client.Get(url)
		if err != nil {
			logrus.Errorf("Get user: %v followers error", userId)
			return
		}

		defer resp.Body.Close()
		var r map[string]interface{}
		err = json.NewDecoder(resp.Body).Decode(&r)
		if err != nil {
			logrus.Errorf("Parse user: %v followers error", userId)
			return
		}

		data, ok := r["data"].([]interface{})
		if !ok {
			logrus.Errorf("Parse user: %v followers error", userId)
			return
		}
		for _, u := range data {
			user, ok := u.(map[string]interface{})
			if !ok {
				logrus.Errorf("Parse user: %v followers error", userId)
				return
			}
			res = append(res, user["url_token"].(string))
		}

		paging, ok := r["paging"].(map[string]interface{})
		if !ok {
			logrus.Errorf("Parse user: %v followers error", userId)
			return
		}
		if paging["is_end"].(bool) == true {
			break
		}
		url, ok = paging["next"].(string)
		if !ok {
			logrus.Errorf("Parse user: %v followers error", userId)
			return
		}
	}
	return
}

func split(x *goquery.Selection) (res []string) {
	var f func(n *html.Node)
	f = func(n *html.Node) {
		if n.Type == html.TextNode {
			res = append(res, n.Data)
		}
		if n.FirstChild != nil {
			for c := n.FirstChild; c != nil; c = c.NextSibling {
				f(c)
			}
		}
	}
	for _, n := range x.Nodes {
		f(n)
	}
	return
}

func GetUserProfile(userId string, client *http.Client) (*types.User, error) {
	logrus.Infof("Start crawling user:%v profile.", userId)
	sleepRandom()
	url := "https://www.zhihu.com/people/" + userId
	resp, err := client.Get(url)
	if err != nil {
		logrus.Infof("Get user: %v profile error: %v", userId, err)
		return nil, err
	}
	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		logrus.Infof("Get user: %v profile error: %v", userId, err)
		return nil, err
	}

	user := types.User{
		Id: userId,
	}
	user.Name = doc.Selection.Find(".ProfileHeader-name").Text()
	user.Headline = doc.Selection.Find(".ProfileHeader-headline").Text()

	info := doc.Selection.Find(".ProfileHeader-infoItem")
	if info.Size() == 2 {
		job := info.First()
		edu := job.Next()
		jobs := split(job)
		edus := split(edu)
		if len(jobs) > 0 {
			user.Industry = jobs[0]
		}
		if len(jobs) > 1 {
			user.Company = jobs[1]
		}
		if len(jobs) > 2 {
			user.Position = jobs[2]
		}
		if len(edus) > 0 {
			user.College = edus[0]
		}
		if len(edus) > 1 {
			user.Major = edus[1]
		}
	}

	doc.Selection.Find("meta").Each(func(i int, s *goquery.Selection) {
		prop, ok := s.Attr("itemprop")
		content, oj := s.Attr("content")
		if ok && oj {
			switch prop {
			case "gender":
				user.Gender = content
			case "image":
				if user.Avatar == "" {
					user.Avatar = content
				}
			case "zhihu:voteupCount":
				user.Voteup, _ = strconv.ParseInt(content, 10, 64)
			case "zhihu:thankedCount":
				user.Thanked, _ = strconv.ParseInt(content, 10, 64)
			case "zhihu:followerCount":
				user.Follower, _ = strconv.ParseInt(content, 10, 64)
			case "zhihu:answerCount":
				user.Answer, _ = strconv.ParseInt(content, 10, 64)
			case "zhihu:articlesCount":
				user.Articles, _ = strconv.ParseInt(content, 10, 64)
			default:
			}
		}
	})
	return &user, nil
}
