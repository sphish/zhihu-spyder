package crawler

import (
	"fmt"
	"testing"
	"zhihu-spyder/client"
)

func TestGetUserFollowees(t *testing.T) {
	userId := "wulala979"
	client, _ := client.GetClient()
	followees := GetUserFollowees(userId, client)
	fmt.Println(followees)
}
func TestGetUserProfile(t *testing.T) {
	userId := "wulala979"
	client, err := client.GetClient()
	if err == nil {
		user, err := GetUserProfile(userId, client)
		fmt.Println(user)
		if err != nil {
			t.Error(err)
		}
	}
}
func TestGetUserFollowers(t *testing.T) {
	userId := "wulala979"
	client, _ := client.GetClient()
	followers := GetUserFollowers(userId, client)
	fmt.Println(followers)
}
