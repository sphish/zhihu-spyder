package dao

import (
	"time"
	"zhihu-spyder/types"
	"zhihu-spyder/util"

	"github.com/Sirupsen/logrus"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var db **gorm.DB

func init() {
	if db == nil {
		tmpDB, err := openDB()
		if err == nil {
			db = &tmpDB
		} else {
			// ensure db is not nil after init function
			panic(err)
		}
		go reconnectDB()
	}
}

func openDB() (*gorm.DB, error) {
	var err error
	resDB, err := gorm.Open("mysql", "root:root123@tcp(localhost:3306)/zhihu_spyder?charset=utf8&parseTime=True")
	if err != nil {
		logrus.Error("open db error, error %v", err)
	} else {
		if util.Env("log_level", "info") == "debug" {
			resDB.LogMode(true)
		}
	}
	return resDB, err
}

func reconnectDB() {
	ticker := time.NewTicker(5 * time.Second)
	for range ticker.C {
		if db != nil {
			if err := (*db).DB().Ping(); err != nil {
				logrus.Warn("db connection lost, reconnect db")
				(*db).DB().Close()
				tmpDB, err := openDB()
				if err == nil {
					*db = tmpDB
					logrus.Infof("new db addr %v", *db)
				}
			}
		} else {
			panic("db should not be nil in reconnect function")
		}
	}
}

func GetDbConnection() **gorm.DB {
	if (!(*db).HasTable(&types.User{})) {
		createDB(db)
	}
	return db
}
