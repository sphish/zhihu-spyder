package dao

import (
	"zhihu-spyder/types"

	"github.com/jinzhu/gorm"
)

func createDB(db **gorm.DB) {
	if err := (*db).Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4").CreateTable(&types.User{}).Error; err != nil {
		panic(err)
	}
}
