package dao

import (
	"zhihu-spyder/types"

	"github.com/jinzhu/gorm"
)

var DB **gorm.DB

func init() {
	DB = GetDbConnection()
}

func CreateUserInfo(userInfo *types.User) error {
	return (*DB).Create(userInfo).Error
}
