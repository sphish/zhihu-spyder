# encoding:utf-8

import pandas

class csv_writer(object):

    def __init__(self, data):
        self.data = data

    def counter_writer(self, str):
        lst = self.data[str]
        dic = {}
        for items in lst:
            if items in dic.keys():
                dic[items] += 1
            else:
                dic[items] = 1
        dic = dict(sorted(dic.items(), key=lambda x: x[1], reverse=True))
        writer = pandas.DataFrame({str[0].upper() + str[1:]: pandas.Series(list(dic.keys())), 'Num': pandas.Series(list(dic.values()))})
        writer.to_csv('processed_data/' + str + '.csv', index=False, sep=',')

    def personal_writer(self, str):
        names = self.data['name']
        str_lst = self.data[str]
        lst = []
        for i in range(len(names)):
            lst.append((names[i], str_lst[i]))
        lst = dict(sorted(lst, key=lambda x:x[1], reverse=True))
        writer = pandas.DataFrame({'Name': pandas.Series(list(lst.keys())), str + 'Num': pandas.Series(list(lst.values()))})
        writer.to_csv('processed_data/' + str + '.csv', index=False, sep=',')