package main

import (
	"encoding/gob"
	"fmt"
	"os"
	"sync"
	"zhihu-spyder/client"
	"zhihu-spyder/crawler"
	"zhihu-spyder/dao"
	"zhihu-spyder/util"

	"github.com/Sirupsen/logrus"
	cuckoo "github.com/joeshaw/cuckoofilter"
)

var (
	startUser  = util.Env("START_USER", "ye-yu-ji-bei-11-49")
	filterSize = util.EnvUint("FILTER_SIZE", 800000)
	filter     *cuckoo.Filter
	queueSize  = util.EnvUint("QUEUE_SIZE", 200000)
	usersChan  = make(chan string, queueSize)
	mutex      sync.RWMutex
)

func clean() {
	close(usersChan)
	db := dao.GetDbConnection()
	(*db).Close()
	file, err := os.Create("cuckoo.gob")
	if err != nil {
		logrus.Errorf("Create gob file error: %v", err)
	}
	encoder := gob.NewEncoder(file)
	err = encoder.Encode(&filter)
	if err != nil {
		logrus.Errorf("Encode gob file error: %v", err)
	}
}

func sendQueue(users []string) {
	for _, user := range users {
		if !filter.Contains([]byte(user)) {
			filter.Add([]byte(user))
			usersChan <- user
		}
	}
}

func crawlOne(userId string) {
	cli, err := client.GetClient()
	if err != nil {
		logrus.Errorf("Get Client Error: %v", err)
		return
	}
	userInfo, err := crawler.GetUserProfile(userId, cli)
	if err != nil {
		logrus.Errorf("Request Error: %v", err)
	} else {
		fmt.Println(userInfo)
		dao.CreateUserInfo(userInfo)
	}

	followers := crawler.GetUserFollowers(userId, cli)
	followees := crawler.GetUserFollowees(userId, cli)

	sendQueue(followees)
	sendQueue(followers)
}

func loadCuckoo() {
	filter = cuckoo.New(uint32(filterSize))
}

func main() {
	//defer clean()

	loadCuckoo()
	sendQueue([]string{startUser})
	loop := func(_ int) {
		for user := range usersChan {
			crawlOne(user)
			logrus.Infof("End crawling user: %v", user)
		}
	}

	concurrency := int(util.EnvInt("CONCURRENCY", 3))
	util.Parallelize(concurrency, concurrency, loop)
}
