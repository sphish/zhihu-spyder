# 项目报告

## 简介

我们的主要工作是用Golang开发一个利用Cuckoo Filter去重的爬取知乎用户信息的爬虫，然后对爬取的信息做简单分析。

项目地址：https://bitbucket.org/sphish/zhihu-spyder

开发者：周尚彦、李拙

任务分配：

* 周尚彦(1600012938): 构建知乎爬虫的框架，爬虫主要部分的编写、调试
* 李拙(1600012911): 爬取免费代理IP，运行知乎爬虫，对爬取数据的管理和分析

基本思路是从一个用户的首页出发，获取页面信息之后，解析followers和followees，将这些用户的页面地址加入到URL队列中，然后不断地从队列中获取URL进行爬取，再将解析得到的followers和followees的URL加入队列中，如此继续下去。

为了避免重复爬取相同的用户，我们使用Cuckoo Filter来去重。

为防止被知乎的服务器识别出爬虫而拒绝服务，我们从网站上爬取了一些免费代理ip并从阿布云购买了一些代理ip，然后在爬虫中使用这些代理ip。

我们将爬到的数据存储到MySQL数据库中，使得数据管理更加规范、通用。

之后我们实现了并发以提高爬虫的效率。

在将爬取的数据存储到MySQL后，我们将数据导出为csv格式以便于用python处理，然后对数据做一些简单的统计和分析。

流程图如下：

![task](pics/task.png)

## 使用方法

### MySQL
默认配置为MySQL地址为localhost:3306，用户名密码为root:root123，需自行创建zhihu-spyder database
```sql
create DATABASE zhihu-spyder;
```
### proxy
将proxys的URL填在client/proxy.json中即可

P.S. 空URL代表不使用代理
### Parameters
使用环境变量配置运行时参数，可配置参数如下:
| Name             | Meaning                                          | Default |
| ---------------- | ------------------------------------------------ | ------- |
| START_USER       | 爬虫起始用户ID，建议每次重启时更换               |         |
| FILTER_SIZE      | Cuckoo Filter的最大容量                          | 800000  |
| QUEUE_SIZE       | UserId Queue的最大长度，超出最大长度时会阻塞进程 | 200000  |
| CONCURRENCY      | 并发goroutine数量                                | 3       |
| log_level        | log等级                                          | info    |
| REQUEST_INTERVAL | 请求间隔(ms)  | 200ms |

## 爬虫

### Crawler
爬虫的主要部分

### Script
创建MySQL数据库

### Types
利用Gorm定义数据类型

### Dao
与MySQL建立连接并利用Gorm进行数据类型的转换

### Util
环境变量
实现并发

### Client
为了防止反爬虫，设置代理ip

### Xiciip

我们利用Scrapy框架，在[http://www.xicidaili.com](http://www.xicidaili.com)上爬取一些免费代理ip。方法是直接向西刺代理服务器发送request，然后使用正则表达式匹配获取网站的代理ip,端口号port和协议HTTP/HTTPS。

Scrapy框架中:

Scrapy Engine负责各个组件的通信、数据传递；

Scheduler负责接受Engine发送的request请求，整理入队，并在Engine获取请求时出队；

Downloader负责下载Engine的request请求，并将response答复交还Engine，再由Engine交给Spider处理；

Spider负责从response中分析数据，解析需要存储的Item数据，并解析新的URL和request交给Engine，由Engine发送给Scheduler；

Item Pipeline负责处理Spider提取的Item数据，然后将数据写入文件或是存入数据库；

Downloader Middlewares和Spider Middlewares是两者的中间件，可以在这里扩充下载功能或爬虫功能，比如设置下载时间间隔和设置爬虫代理服务等。


![scrapy_arch](pics/scrapy_architecture.png)

我们的具体工作如下;

* 在items.py中利用scrapy的Item对象定义数据类型，也就是需要爬取的ip,port,http字段
* xiqiip.py是主要的爬虫部分，利用request请求获取网页信息，用正则表达式匹配字段，返回爬取的对象
* 在pipelines.py中处理爬取的对象，使用Telnet或request测试爬到的代理ip是否可用，将可用的代理ip写入ips.json
* 为了避免网站的反爬虫，在middlewares.py--中间组件--中配置了request headers和proxy ip
* settings.py是相关的一些设置

我们从网站上爬取了2k多条代理ip信息，经过初步测试只有不到2%的可以使用，将一些可用的代理ip保存在client/proxy_.json中，进行实际运行测试。在爬取知乎页面信息时发现代理ip不仅速度不快，还经常遭到知乎服务器的封禁，并且不稳定，经常失效。为了爬取足够的数据，我们从阿布云购买了一些代理ip以供使用。

## 运行

实际运行的效果图如下：

![spider](pics/spider.png)

## 数据处理与分析

这里的分析只针对我们爬取的2万余条用户信息，可能不具有代表性。

在将数据存储到MySQL中之后，将数据导出为csv格式。然后在data_process/read.py和data_process/write.py中做一些简单的处理，写入data_process/processed_data中。

对处理之后得到的文件，用Excel画出简单的可视化图表：分析知乎用户的性别比例，从事的工作，就读的大学，工作的公司，然后统计获得点赞、感谢和关注者最多的一些用户。

*我们先看一看用户整体的分布情况*

### 性别

可以看到，知乎用户中男性显著多余女性。

![gender](pics/gender.png)

### 职业

知乎用户中从事最多的是互联网行业

![industry](pics/industry.png)

### 公司

任职公司主要除BAT(Baidu, Alibaba, Tencent)之外，还有知乎、Facebook、Google等

![company](pics/company.png)

### 教育

可以看到，在填写了教育经历的用户中，大部分是清北的学生，然后是复旦和上交，可见本科大学比较好的用户才原意填写自己的教育经历

![college](pics/college.png)

*接下来看一下较为活跃的一些用户*

### 获赞数

获赞最多的是著名写手--张佳玮，vzch紧随其后

![voteup](pics/voteup.png)

### 感谢数

获得感谢数最多的用户同样是张佳玮

![thanked](pics/thanked.png)

### 关注者

关注者最多的用户依然是张佳玮...

![follower](pics/follower.png)



