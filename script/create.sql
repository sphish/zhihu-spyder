CREATE DATABASE zhihu_spyder;
use zhihu_spyder;

CREATE TABLE `users` (
  `index` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(255) NOT NULL,
  `zhihu_id` VARCHAR(255) NOT NULL,
  `headline` VARCHAR(255) NOT NULL,
  `gender` VARCHAR(255) NOT NULL,
  `avatar` VARCHAR(255) NOT NULL,
  `user_type` VARCHAR(255) NOT NULL,
  `industry` VARCHAR(255) NOT NULL,
  `company` VARCHAR(255) NOT NULL,
  `position` VARCHAR(255) NOT NULL,
  `college` VARCHAR(255) NOT NULL,
  `major` VARCHAR(255) NOT NULL,
  `voteup` TINYINT(11) DEFAULT 0,
  `thanked` TINYINT(11) DEFAULT 0,
  `follower` TINYINT(11) DEFAULT 0,
  `answer` TINYINT(11) DEFAULT 0,
  `articles` TINYINT(11) DEFAULT 0
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE INDEX user_info_voteup_index ON user(`voteup`);
CREATE INDEX user_info_thanked_index ON user(`thanked`);
CREATE INDEX user_info_follower_index ON user(`follower`);
CREATE INDEX user_info_answer_index ON user(`answer`);
CREATE INDEX user_info_articles_index ON user(`articles`);
