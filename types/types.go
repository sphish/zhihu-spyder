package types

type User struct {
	Name     string
	Id       string
	Headline string
	Gender   string
	Avatar   string
	Industry string
	Company  string
	Position string
	College  string
	Major    string
	Voteup   int64
	Thanked  int64
	Follower int64
	Answer   int64
	Articles int64
}
