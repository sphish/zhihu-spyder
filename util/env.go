package util

import (
	"os"
	"strconv"
	"time"
)

func Env(key, defaultValue string) (value string) {
	if value = os.Getenv(key); value == "" {
		value = defaultValue
	}
	return
}

func EnvUint(key string, defaultValue uint64) uint64 {
	valueStr := Env(key, "")
	if valueStr == "" {
		return defaultValue
	} else {
		value, err := strconv.ParseUint(valueStr, 10, 64)
		if err != nil {
			panic(err.Error())
		}
		return value
	}
}

func EnvInt(key string, defaultValue int32) int32 {
	valueStr := Env(key, "")
	if valueStr == "" {
		return defaultValue
	} else {
		value, err := strconv.ParseInt(valueStr, 10, 32)
		if err != nil {
			panic(err.Error())
		}
		return int32(value)
	}
}

func EnvDuration(key, defaultValue string) time.Duration {
	value, err := time.ParseDuration(Env(key, defaultValue))
	if err != nil {
		panic(err.Error())
	}
	return value
}

func EnvFloat(key, defaultValue string) float64 {
	value, err := strconv.ParseFloat(Env(key, defaultValue), 64)
	if err != nil {
		panic(err.Error())
	}
	return value
}

func EnvBool(key string, defaultValue bool) bool {
	valueStr := Env(key, "")
	if valueStr == "" {
		return defaultValue
	} else {
		value, err := strconv.ParseBool(valueStr)
		if err != nil {
			panic(err.Error())
		}
		return value
	}
}
