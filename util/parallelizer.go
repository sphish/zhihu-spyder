package util

import (
	"sync"
)

type DoWorkPieceFunc func(piece int)

func Parallelize(workers, pieces int, doWorkPiece DoWorkPieceFunc) {
	if pieces < workers {
		workers = pieces
	}

	toProcess := make(chan int, workers)
	go func() {
		for i := 0; i < pieces; i++ {
			toProcess <- i
		}
		close(toProcess)
	}()

	wg := sync.WaitGroup{}
	wg.Add(workers)
	panicCh := make(chan interface{}, workers)
	for i := 0; i < workers; i++ {
		go func() {
			defer func() {
				if p := recover(); p != nil {
					panicCh <- p
				}
			}()
			defer wg.Done()
			for piece := range toProcess {
				doWorkPiece(piece)
			}
		}()
	}
	wg.Wait()

	select {
	case p := <-panicCh:
		panic(p)
	default:
		return
	}
}
