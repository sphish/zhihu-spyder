package util

import "testing"

func TestParallelize(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Logf("handler panic %v", r)
		}
	}()

	workFunc := func(i int) {
		t.Logf("%v is running", i)
		panic("for test")
	}
	Parallelize(4, 4, workFunc)
}
