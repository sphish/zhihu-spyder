import telnetlib

input = open('ips0.json', 'r')
lines = input.readlines()
input.close()
lines = [eval(line[:-1]) for line in lines]
lines = [line for line in lines if line['type'] == 'HTTPS']

output = open('available.json', 'w')

good = 0
bad = 0

avai = []

for line in lines:
    try:
        telnetlib.Telnet(line['ip'], port=line['port'], timeout=5)
    except:
        bad = bad + 1
        print(line['ip'] + ' connect failed')
    else:
        good = good + 1
        avai.append('https://' + line['ip'] + ':' + line['port'])
        print(line['ip'] + ' success')

print("good: ", good)
print("bad: ", bad)
print(1.0 * good / (good + bad) * 100)

output.write('[\n')
for l in avai:
    output.write('\t"' + l + '",\n')
output.write(']\n')
output.close()