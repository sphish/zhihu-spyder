# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import json
import codecs
import requests

class XiqiipPipeline(object):

    def __init__(self):
        self.file = codecs.open('ips.json', 'w', encoding='utf-8')
        self.file.write('[\n')

    def process_item(self, item, spider):
        # return item
        # lines = json.dumps(dict(item), ensure_ascii=False)
        lines = dict(item)
        try:
            print("https://" + lines['ip'] + ':' + lines['port'])
            req = requests.get('https://www.zhihu.com', proxies={"https": "https://" + lines['ip'] + ':' + lines['port']})
        except:
            print('connection error')
        else:
            print('success', req)
            self.file.write('\t"' + 'https://' + lines['ip'] + ':' + lines['port'] + '",\n')
        return item

    def close_spider(self, spider):
        self.file.write(']\n')
        self.file.close()
