# -*- coding: utf-8 -*-
import scrapy
import random
import re
from bs4 import BeautifulSoup
from scrapy.http import Request
from xiqiip.items import XiqiipItem
from scrapy.spiders import CrawlSpider

class XiqiipSpider(CrawlSpider):
    name = 'xiqiip'
    allowed_domains = ['xicidaili.com']
    start_urls = 'http://www.xicidaili.com/wn/'
    # name = 'dingdian'
    # allowed_domains = ['23us.so']
    # bash_url = 'http://www.23us.so/list/'
    # bashurl = '.html'

    def start_requests(self):
        # headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"}
        # headers = random.choice(xiqiip.spiders.USER_AGENTS_LIST)
        # for i in range(1, 3356):
        for i in range(1, 2):
            url = self.start_urls + str(i)
            yield Request(url, self.parse)

    def parse(self, response):
        # print(response.text)
        # trs = BeautifulSoup(response.text, 'lxml').find_all('td')
        # for tr in trs:
        #     for a in tr:
        #         print(a)
        #     # break
        pattern_ip = re.compile('<td>\d+.\d+.\d+.\d+</td>')
        pattern_port = re.compile('<td>\d+</td>')
        pattern_xieyi = re.compile('<td>[A-Z]*</td>')
        ips = re.findall(pattern_ip ,response.text)
        ports = re.findall(pattern_port, response.text)
        xieyis = re.findall(pattern_xieyi, response.text)
        if not len(ips) == len(ports):
            exit(1)
        if not len(ports) == len(xieyis):
            exit(1)
        for i in range(0, 20):
            # print(ips[i][4:-5], ports[i][4:-5], xieyis[i][4:-5])
            item = XiqiipItem()
            item['ip'] = ips[i][4:-5]
            item['port'] = ports[i][4:-5]
            item['type'] = xieyis[i][4:-5]
            yield item
